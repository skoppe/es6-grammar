# ES6 Grammar

This library contains an ECMAScript6 + JSX Parser written in [D](http://dlang.org).

The Parser is generated using [Pegged](https://github.com/PhilippeSigaud/Pegged).

# Future

The idea is to write a ES6 -> ES5 transpiler, a linter and a minifier on top of it.

# Regenerating the Parser

The package has a pre-build parser, but in case you want to fork and modify, the complete grammar can be found in the `source/es6/grammar-generator.d` file. Run `dub --config=generate` to generate the `source/es6/grammar.d` file.