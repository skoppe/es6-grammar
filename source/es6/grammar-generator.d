module es6.grammargenerator;
// Note: ID_Continue should also include the following chars but dmd complains:
//      ("\u0001" [\uda00-\uda36]) / ("\u0001" [\uda3b-\uda6c]) / ("\u0001\uda75") / ("\u0001\uda84") / ("\u0001" [\uda9b-\uda9f]) / ("\u0001" [\udaa1-\udaaf]) /

// Note: PostfixExpression and PostfixExpressionYield are wrong, there should be no LineTerminators between LeftHandSideExpression/LeftHandSideExpressionYield and the ++/--
// Note: Identifier cannot be one of ReservedWord, but need to do AFTER parsing in semantic analyses
// Note: The following rules have been simplified, originally they were:
//      ID_Start        <- [\u0041-\u005a] / [\u0061-\u007a] / "\u00aa" / "\u00b5" / "\u00ba" / [\u00c0-\u00d6] / [\u00d8-\u00f6] / [\u00f8-\u02c1] / [\u02c6-\u02d1] / [\u02e0-\u02e4] / "\u02ec" / "\u02ee" / [\u0370-\u0374] / [\u0376-\u0377] / [\u037a-\u037d] / "\u037f" / "\u0386" / [\u0388-\u038a] / "\u038c" / [\u038e-\u03a1] / [\u03a3-\u03f5] / [\u03f7-\u0481] / [\u048a-\u052f] / [\u0531-\u0556] / "\u0559" / [\u0561-\u0587] / [\u05d0-\u05ea] / [\u05f0-\u05f2] / [\u0620-\u064a] / [\u066e-\u066f] / [\u0671-\u06d3] / "\u06d5" / [\u06e5-\u06e6] / [\u06ee-\u06ef] / [\u06fa-\u06fc] / "\u06ff" / "\u0710" / [\u0712-\u072f] / [\u074d-\u07a5] / "\u07b1" / [\u07ca-\u07ea] / [\u07f4-\u07f5] / "\u07fa" / [\u0800-\u0815] / "\u081a" / "\u0824" / "\u0828" / [\u0840-\u0858] / [\u08a0-\u08b4] / [\u0904-\u0939] / "\u093d" / "\u0950" / [\u0958-\u0961] / [\u0971-\u0980] / [\u0985-\u098c] / [\u098f-\u0990] / [\u0993-\u09a8] / [\u09aa-\u09b0] / "\u09b2" / [\u09b6-\u09b9] / "\u09bd" / "\u09ce" / [\u09dc-\u09dd] / [\u09df-\u09e1] / [\u09f0-\u09f1] / [\u0a05-\u0a0a] / [\u0a0f-\u0a10] / [\u0a13-\u0a28] / [\u0a2a-\u0a30] / [\u0a32-\u0a33] / [\u0a35-\u0a36] / [\u0a38-\u0a39] / [\u0a59-\u0a5c] / "\u0a5e" / [\u0a72-\u0a74] / [\u0a85-\u0a8d] / [\u0a8f-\u0a91] / [\u0a93-\u0aa8] / [\u0aaa-\u0ab0] / [\u0ab2-\u0ab3] / [\u0ab5-\u0ab9] / "\u0abd" / "\u0ad0" / [\u0ae0-\u0ae1] / "\u0af9" / [\u0b05-\u0b0c] / [\u0b0f-\u0b10] / [\u0b13-\u0b28] / [\u0b2a-\u0b30] / [\u0b32-\u0b33] / [\u0b35-\u0b39] / "\u0b3d" / [\u0b5c-\u0b5d] / [\u0b5f-\u0b61] / "\u0b71" / "\u0b83" / [\u0b85-\u0b8a] / [\u0b8e-\u0b90] / [\u0b92-\u0b95] / [\u0b99-\u0b9a] / "\u0b9c" / [\u0b9e-\u0b9f] / [\u0ba3-\u0ba4] / [\u0ba8-\u0baa] / [\u0bae-\u0bb9] / "\u0bd0" / [\u0c05-\u0c0c] / [\u0c0e-\u0c10] / [\u0c12-\u0c28] / [\u0c2a-\u0c39] / "\u0c3d" / [\u0c58-\u0c5a] / [\u0c60-\u0c61] / [\u0c85-\u0c8c] / [\u0c8e-\u0c90] / [\u0c92-\u0ca8] / [\u0caa-\u0cb3] / [\u0cb5-\u0cb9] / "\u0cbd" / "\u0cde" / [\u0ce0-\u0ce1] / [\u0cf1-\u0cf2] / [\u0d05-\u0d0c] / [\u0d0e-\u0d10] / [\u0d12-\u0d3a] / "\u0d3d" / "\u0d4e" / [\u0d5f-\u0d61] / [\u0d7a-\u0d7f] / [\u0d85-\u0d96] / [\u0d9a-\u0db1] / [\u0db3-\u0dbb] / "\u0dbd" / [\u0dc0-\u0dc6] / [\u0e01-\u0e30] / [\u0e32-\u0e33] / [\u0e40-\u0e46] / [\u0e81-\u0e82] / "\u0e84" / [\u0e87-\u0e88] / "\u0e8a" / "\u0e8d" / [\u0e94-\u0e97] / [\u0e99-\u0e9f] / [\u0ea1-\u0ea3] / "\u0ea5" / "\u0ea7" / [\u0eaa-\u0eab] / [\u0ead-\u0eb0] / [\u0eb2-\u0eb3] / "\u0ebd" / [\u0ec0-\u0ec4] / "\u0ec6" / [\u0edc-\u0edf] / "\u0f00" / [\u0f40-\u0f47] / [\u0f49-\u0f6c] / [\u0f88-\u0f8c] / [\u1000-\u102a] / "\u103f" / [\u1050-\u1055] / [\u105a-\u105d] / "\u1061" / [\u1065-\u1066] / [\u106e-\u1070] / [\u1075-\u1081] / "\u108e" / [\u10a0-\u10c5] / "\u10c7" / "\u10cd" / [\u10d0-\u10fa] / [\u10fc-\u1248] / [\u124a-\u124d] / [\u1250-\u1256] / "\u1258" / [\u125a-\u125d] / [\u1260-\u1288] / [\u128a-\u128d] / [\u1290-\u12b0] / [\u12b2-\u12b5] / [\u12b8-\u12be] / "\u12c0" / [\u12c2-\u12c5] / [\u12c8-\u12d6] / [\u12d8-\u1310] / [\u1312-\u1315] / [\u1318-\u135a] / [\u1380-\u138f] / [\u13a0-\u13f5] / [\u13f8-\u13fd] / [\u1401-\u166c] / [\u166f-\u167f] / [\u1681-\u169a] / [\u16a0-\u16ea] / [\u16ee-\u16f8] / [\u1700-\u170c] / [\u170e-\u1711] / [\u1720-\u1731] / [\u1740-\u1751] / [\u1760-\u176c] / [\u176e-\u1770] / [\u1780-\u17b3] / "\u17d7" / "\u17dc" / [\u1820-\u1877] / [\u1880-\u18a8] / "\u18aa" / [\u18b0-\u18f5] / [\u1900-\u191e] / [\u1950-\u196d] / [\u1970-\u1974] / [\u1980-\u19ab] / [\u19b0-\u19c9] / [\u1a00-\u1a16] / [\u1a20-\u1a54] / "\u1aa7" / [\u1b05-\u1b33] / [\u1b45-\u1b4b] / [\u1b83-\u1ba0] / [\u1bae-\u1baf] / [\u1bba-\u1be5] / [\u1c00-\u1c23] / [\u1c4d-\u1c4f] / [\u1c5a-\u1c7d] / [\u1ce9-\u1cec] / [\u1cee-\u1cf1] / [\u1cf5-\u1cf6] / [\u1d00-\u1dbf] / [\u1e00-\u1f15] / [\u1f18-\u1f1d] / [\u1f20-\u1f45] / [\u1f48-\u1f4d] / [\u1f50-\u1f57] / "\u1f59" / "\u1f5b" / "\u1f5d" / [\u1f5f-\u1f7d] / [\u1f80-\u1fb4] / [\u1fb6-\u1fbc] / "\u1fbe" / [\u1fc2-\u1fc4] / [\u1fc6-\u1fcc] / [\u1fd0-\u1fd3] / [\u1fd6-\u1fdb] / [\u1fe0-\u1fec] / [\u1ff2-\u1ff4] / [\u1ff6-\u1ffc] / "\u2071" / "\u207f" / [\u2090-\u209c] / "\u2102" / "\u2107" / [\u210a-\u2113] / "\u2115" / [\u2118-\u211d] / "\u2124" / "\u2126" / "\u2128" / [\u212a-\u2139] / [\u213c-\u213f] / [\u2145-\u2149] / "\u214e" / [\u2160-\u2188] / [\u2c00-\u2c2e] / [\u2c30-\u2c5e] / [\u2c60-\u2ce4] / [\u2ceb-\u2cee] / [\u2cf2-\u2cf3] / [\u2d00-\u2d25] / "\u2d27" / "\u2d2d" / [\u2d30-\u2d67] / "\u2d6f" / [\u2d80-\u2d96] / [\u2da0-\u2da6] / [\u2da8-\u2dae] / [\u2db0-\u2db6] / [\u2db8-\u2dbe] / [\u2dc0-\u2dc6] / [\u2dc8-\u2dce] / [\u2dd0-\u2dd6] / [\u2dd8-\u2dde] / [\u3005-\u3007] / [\u3021-\u3029] / [\u3031-\u3035] / [\u3038-\u303c] / [\u3041-\u3096] / [\u309b-\u309f] / [\u30a1-\u30fa] / [\u30fc-\u30ff] / [\u3105-\u312d] / [\u3131-\u318e] / [\u31a0-\u31ba] / [\u31f0-\u31ff] / [\u3400-\u4db5] / [\u4e00-\u9fd5] / [\ua000-\ua48c] / [\ua4d0-\ua4fd] / [\ua500-\ua60c] / [\ua610-\ua61f] / [\ua62a-\ua62b] / [\ua640-\ua66e] / [\ua67f-\ua69d] / [\ua6a0-\ua6ef] / [\ua717-\ua71f] / [\ua722-\ua788] / [\ua78b-\ua7ad] / [\ua7b0-\ua7b7] / [\ua7f7-\ua801] / [\ua803-\ua805] / [\ua807-\ua80a] / [\ua80c-\ua822] / [\ua840-\ua873] / [\ua882-\ua8b3] / [\ua8f2-\ua8f7] / "\ua8fb" / "\ua8fd" / [\ua90a-\ua925] / [\ua930-\ua946] / [\ua960-\ua97c] / [\ua984-\ua9b2] / "\ua9cf" / [\ua9e0-\ua9e4] / [\ua9e6-\ua9ef] / [\ua9fa-\ua9fe] / [\uaa00-\uaa28] / [\uaa40-\uaa42] / [\uaa44-\uaa4b] / [\uaa60-\uaa76] / "\uaa7a" / [\uaa7e-\uaaaf] / "\uaab1" / [\uaab5-\uaab6] / [\uaab9-\uaabd] / "\uaac0" / "\uaac2" / [\uaadb-\uaadd] / [\uaae0-\uaaea] / [\uaaf2-\uaaf4] / [\uab01-\uab06] / [\uab09-\uab0e] / [\uab11-\uab16] / [\uab20-\uab26] / [\uab28-\uab2e] / [\uab30-\uab5a] / [\uab5c-\uab65] / [\uab70-\uabe2] / [\uac00-\ud7a3] / [\ud7b0-\ud7c6] / [\ud7cb-\ud7fb] / [\uf900-\ufa6d] / [\ufa70-\ufad9] / [\ufb00-\ufb06] / [\ufb13-\ufb17] / "\ufb1d" / [\ufb1f-\ufb28] / [\ufb2a-\ufb36] / [\ufb38-\ufb3c] / "\ufb3e" / [\ufb40-\ufb41] / [\ufb43-\ufb44] / [\ufb46-\ufbb1] / [\ufbd3-\ufd3d] / [\ufd50-\ufd8f] / [\ufd92-\ufdc7] / [\ufdf0-\ufdfb] / [\ufe70-\ufe74] / [\ufe76-\ufefc] / [\uff21-\uff3a] / [\uff41-\uff5a] / [\uff66-\uffbe] / [\uffc2-\uffc7] / [\uffca-\uffcf] / [\uffd2-\uffd7] / [\uffda-\uffdc] / ("\u0001" [\u0000-\u000b]) / ("\u0001" [\u000d-\u0026]) / ("\u0001" [\u0028-\u003a]) / ("\u0001" [\u003c-\u003d]) / ("\u0001" [\u003f-\u004d]) / ("\u0001" [\u0050-\u005d]) / ("\u0001" [\u0080-\u00fa]) / ("\u0001" [\u0140-\u0174]) / ("\u0001" [\u0280-\u029c]) / ("\u0001" [\u02a0-\u02d0]) / ("\u0001" [\u0300-\u031f]) / ("\u0001" [\u0330-\u034a]) / ("\u0001" [\u0350-\u0375]) / ("\u0001" [\u0380-\u039d]) / ("\u0001" [\u03a0-\u03c3]) / ("\u0001" [\u03c8-\u03cf]) / ("\u0001" [\u03d1-\u03d5]) / ("\u0001" [\u0400-\u049d]) / ("\u0001" [\u0500-\u0527]) / ("\u0001" [\u0530-\u0563]) / ("\u0001" [\u0600-\u0736]) / ("\u0001" [\u0740-\u0755]) / ("\u0001" [\u0760-\u0767]) / ("\u0001" [\u0800-\u0805]) / ("\u0001\u0808") / ("\u0001" [\u080a-\u0835]) / ("\u0001" [\u0837-\u0838]) / ("\u0001\u083c") / ("\u0001" [\u083f-\u0855]) / ("\u0001" [\u0860-\u0876]) / ("\u0001" [\u0880-\u089e]) / ("\u0001" [\u08e0-\u08f2]) / ("\u0001" [\u08f4-\u08f5]) / ("\u0001" [\u0900-\u0915]) / ("\u0001" [\u0920-\u0939]) / ("\u0001" [\u0980-\u09b7]) / ("\u0001" [\u09be-\u09bf]) / ("\u0001\u0a00") / ("\u0001" [\u0a10-\u0a13]) / ("\u0001" [\u0a15-\u0a17]) / ("\u0001" [\u0a19-\u0a33]) / ("\u0001" [\u0a60-\u0a7c]) / ("\u0001" [\u0a80-\u0a9c]) / ("\u0001" [\u0ac0-\u0ac7]) / ("\u0001" [\u0ac9-\u0ae4]) / ("\u0001" [\u0b00-\u0b35]) / ("\u0001" [\u0b40-\u0b55]) / ("\u0001" [\u0b60-\u0b72]) / ("\u0001" [\u0b80-\u0b91]) / ("\u0001" [\u0c00-\u0c48]) / ("\u0001" [\u0c80-\u0cb2]) / ("\u0001" [\u0cc0-\u0cf2]) / ("\u0001" [\u1003-\u1037]) / ("\u0001" [\u1083-\u10af]) / ("\u0001" [\u10d0-\u10e8]) / ("\u0001" [\u1103-\u1126]) / ("\u0001" [\u1150-\u1172]) / ("\u0001\u1176") / ("\u0001" [\u1183-\u11b2]) / ("\u0001" [\u11c1-\u11c4]) / ("\u0001\u11da") / ("\u0001\u11dc") / ("\u0001" [\u1200-\u1211]) / ("\u0001" [\u1213-\u122b]) / ("\u0001" [\u1280-\u1286]) / ("\u0001\u1288") / ("\u0001" [\u128a-\u128d]) / ("\u0001" [\u128f-\u129d]) / ("\u0001" [\u129f-\u12a8]) / ("\u0001" [\u12b0-\u12de]) / ("\u0001" [\u1305-\u130c]) / ("\u0001" [\u130f-\u1310]) / ("\u0001" [\u1313-\u1328]) / ("\u0001" [\u132a-\u1330]) / ("\u0001" [\u1332-\u1333]) / ("\u0001" [\u1335-\u1339]) / ("\u0001\u133d") / ("\u0001\u1350") / ("\u0001" [\u135d-\u1361]) / ("\u0001" [\u1480-\u14af]) / ("\u0001" [\u14c4-\u14c5]) / ("\u0001\u14c7") / ("\u0001" [\u1580-\u15ae]) / ("\u0001" [\u15d8-\u15db]) / ("\u0001" [\u1600-\u162f]) / ("\u0001\u1644") / ("\u0001" [\u1680-\u16aa]) / ("\u0001" [\u1700-\u1719]) / ("\u0001" [\u18a0-\u18df]) / ("\u0001\u18ff") / ("\u0001" [\u1ac0-\u1af8]) / ("\u0001" [\u2000-\u2399]) / ("\u0001" [\u2400-\u246e]) / ("\u0001" [\u2480-\u2543]) / ("\u0001" [\u3000-\u342e]) / ("\u0001" [\u4400-\u4646]) / ("\u0001" [\u6800-\u6a38]) / ("\u0001" [\u6a40-\u6a5e]) / ("\u0001" [\u6ad0-\u6aed]) / ("\u0001" [\u6b00-\u6b2f]) / ("\u0001" [\u6b40-\u6b43]) / ("\u0001" [\u6b63-\u6b77]) / ("\u0001" [\u6b7d-\u6b8f]) / ("\u0001" [\u6f00-\u6f44]) / ("\u0001\u6f50") / ("\u0001" [\u6f93-\u6f9f]) / ("\u0001" [\ub000-\ub001]) / ("\u0001" [\ubc00-\ubc6a]) / ("\u0001" [\ubc70-\ubc7c]) / ("\u0001" [\ubc80-\ubc88]) / ("\u0001" [\ubc90-\ubc99]) / ("\u0001" [\ud400-\ud454]) / ("\u0001" [\ud456-\ud49c]) / ("\u0001" [\ud49e-\ud49f]) / ("\u0001\ud4a2") / ("\u0001" [\ud4a5-\ud4a6]) / ("\u0001" [\ud4a9-\ud4ac]) / ("\u0001" [\ud4ae-\ud4b9]) / ("\u0001\ud4bb") / ("\u0001" [\ud4bd-\ud4c3]) / ("\u0001" [\ud4c5-\ud505]) / ("\u0001" [\ud507-\ud50a]) / ("\u0001" [\ud50d-\ud514]) / ("\u0001" [\ud516-\ud51c]) / ("\u0001" [\ud51e-\ud539]) / ("\u0001" [\ud53b-\ud53e]) / ("\u0001" [\ud540-\ud544]) / ("\u0001\ud546") / ("\u0001" [\ud54a-\ud550]) / ("\u0001" [\ud552-\ud6a5]) / ("\u0001" [\ud6a8-\ud6c0]) / ("\u0001" [\ud6c2-\ud6da]) / ("\u0001" [\ud6dc-\ud6fa]) / ("\u0001" [\ud6fc-\ud714]) / ("\u0001" [\ud716-\ud734]) / ("\u0001" [\ud736-\ud74e]) / ("\u0001" [\ud750-\ud76e]) / ("\u0001" [\ud770-\ud788]) / ("\u0001" [\ud78a-\ud7a8]) / ("\u0001" [\ud7aa-\ud7c2]) / ("\u0001" [\ud7c4-\ud7cb]) / ("\u0001" [\ue800-\ue8c4]) / ("\u0001" [\uee00-\uee03]) / ("\u0001" [\uee05-\uee1f]) / ("\u0001" [\uee21-\uee22]) / ("\u0001\uee24") / ("\u0001\uee27") / ("\u0001" [\uee29-\uee32]) / ("\u0001" [\uee34-\uee37]) / ("\u0001\uee39") / ("\u0001\uee3b") / ("\u0001\uee42") / ("\u0001\uee47") / ("\u0001\uee49") / ("\u0001\uee4b") / ("\u0001" [\uee4d-\uee4f]) / ("\u0001" [\uee51-\uee52]) / ("\u0001\uee54") / ("\u0001\uee57") / ("\u0001\uee59") / ("\u0001\uee5b") / ("\u0001\uee5d") / ("\u0001\uee5f") / ("\u0001" [\uee61-\uee62]) / ("\u0001\uee64") / ("\u0001" [\uee67-\uee6a]) / ("\u0001" [\uee6c-\uee72]) / ("\u0001" [\uee74-\uee77]) / ("\u0001" [\uee79-\uee7c]) / ("\u0001\uee7e") / ("\u0001" [\uee80-\uee89]) / ("\u0001" [\uee8b-\uee9b]) / ("\u0001" [\ueea1-\ueea3]) / ("\u0001" [\ueea5-\ueea9]) / ("\u0001" [\ueeab-\ueebb]) / ("\u0002" [\u0000-\ua6d6]) / ("\u0002" [\ua700-\ub734]) / ("\u0002" [\ub740-\ub81d]) / ("\u0002" [\ub820-\ucea1]) / ("\u0002" [\uf800-\ufa1d])
//      ID_Continue     <- [\u0030-\u0039] / [\u0041-\u005a] / "\u005f" / [\u0061-\u007a] / "\u00aa" / "\u00b5" / "\u00b7" / "\u00ba" / [\u00c0-\u00d6] / [\u00d8-\u00f6] / [\u00f8-\u02c1] / [\u02c6-\u02d1] / [\u02e0-\u02e4] / "\u02ec" / "\u02ee" / [\u0300-\u0374] / [\u0376-\u0377] / [\u037a-\u037d] / "\u037f" / [\u0386-\u038a] / "\u038c" / [\u038e-\u03a1] / [\u03a3-\u03f5] / [\u03f7-\u0481] / [\u0483-\u0487] / [\u048a-\u052f] / [\u0531-\u0556] / "\u0559" / [\u0561-\u0587] / [\u0591-\u05bd] / "\u05bf" / [\u05c1-\u05c2] / [\u05c4-\u05c5] / "\u05c7" / [\u05d0-\u05ea] / [\u05f0-\u05f2] / [\u0610-\u061a] / [\u0620-\u0669] / [\u066e-\u06d3] / [\u06d5-\u06dc] / [\u06df-\u06e8] / [\u06ea-\u06fc] / "\u06ff" / [\u0710-\u074a] / [\u074d-\u07b1] / [\u07c0-\u07f5] / "\u07fa" / [\u0800-\u082d] / [\u0840-\u085b] / [\u08a0-\u08b4] / [\u08e3-\u0963] / [\u0966-\u096f] / [\u0971-\u0983] / [\u0985-\u098c] / [\u098f-\u0990] / [\u0993-\u09a8] / [\u09aa-\u09b0] / "\u09b2" / [\u09b6-\u09b9] / [\u09bc-\u09c4] / [\u09c7-\u09c8] / [\u09cb-\u09ce] / "\u09d7" / [\u09dc-\u09dd] / [\u09df-\u09e3] / [\u09e6-\u09f1] / [\u0a01-\u0a03] / [\u0a05-\u0a0a] / [\u0a0f-\u0a10] / [\u0a13-\u0a28] / [\u0a2a-\u0a30] / [\u0a32-\u0a33] / [\u0a35-\u0a36] / [\u0a38-\u0a39] / "\u0a3c" / [\u0a3e-\u0a42] / [\u0a47-\u0a48] / [\u0a4b-\u0a4d] / "\u0a51" / [\u0a59-\u0a5c] / "\u0a5e" / [\u0a66-\u0a75] / [\u0a81-\u0a83] / [\u0a85-\u0a8d] / [\u0a8f-\u0a91] / [\u0a93-\u0aa8] / [\u0aaa-\u0ab0] / [\u0ab2-\u0ab3] / [\u0ab5-\u0ab9] / [\u0abc-\u0ac5] / [\u0ac7-\u0ac9] / [\u0acb-\u0acd] / "\u0ad0" / [\u0ae0-\u0ae3] / [\u0ae6-\u0aef] / "\u0af9" / [\u0b01-\u0b03] / [\u0b05-\u0b0c] / [\u0b0f-\u0b10] / [\u0b13-\u0b28] / [\u0b2a-\u0b30] / [\u0b32-\u0b33] / [\u0b35-\u0b39] / [\u0b3c-\u0b44] / [\u0b47-\u0b48] / [\u0b4b-\u0b4d] / [\u0b56-\u0b57] / [\u0b5c-\u0b5d] / [\u0b5f-\u0b63] / [\u0b66-\u0b6f] / "\u0b71" / [\u0b82-\u0b83] / [\u0b85-\u0b8a] / [\u0b8e-\u0b90] / [\u0b92-\u0b95] / [\u0b99-\u0b9a] / "\u0b9c" / [\u0b9e-\u0b9f] / [\u0ba3-\u0ba4] / [\u0ba8-\u0baa] / [\u0bae-\u0bb9] / [\u0bbe-\u0bc2] / [\u0bc6-\u0bc8] / [\u0bca-\u0bcd] / "\u0bd0" / "\u0bd7" / [\u0be6-\u0bef] / [\u0c00-\u0c03] / [\u0c05-\u0c0c] / [\u0c0e-\u0c10] / [\u0c12-\u0c28] / [\u0c2a-\u0c39] / [\u0c3d-\u0c44] / [\u0c46-\u0c48] / [\u0c4a-\u0c4d] / [\u0c55-\u0c56] / [\u0c58-\u0c5a] / [\u0c60-\u0c63] / [\u0c66-\u0c6f] / [\u0c81-\u0c83] / [\u0c85-\u0c8c] / [\u0c8e-\u0c90] / [\u0c92-\u0ca8] / [\u0caa-\u0cb3] / [\u0cb5-\u0cb9] / [\u0cbc-\u0cc4] / [\u0cc6-\u0cc8] / [\u0cca-\u0ccd] / [\u0cd5-\u0cd6] / "\u0cde" / [\u0ce0-\u0ce3] / [\u0ce6-\u0cef] / [\u0cf1-\u0cf2] / [\u0d01-\u0d03] / [\u0d05-\u0d0c] / [\u0d0e-\u0d10] / [\u0d12-\u0d3a] / [\u0d3d-\u0d44] / [\u0d46-\u0d48] / [\u0d4a-\u0d4e] / "\u0d57" / [\u0d5f-\u0d63] / [\u0d66-\u0d6f] / [\u0d7a-\u0d7f] / [\u0d82-\u0d83] / [\u0d85-\u0d96] / [\u0d9a-\u0db1] / [\u0db3-\u0dbb] / "\u0dbd" / [\u0dc0-\u0dc6] / "\u0dca" / [\u0dcf-\u0dd4] / "\u0dd6" / [\u0dd8-\u0ddf] / [\u0de6-\u0def] / [\u0df2-\u0df3] / [\u0e01-\u0e3a] / [\u0e40-\u0e4e] / [\u0e50-\u0e59] / [\u0e81-\u0e82] / "\u0e84" / [\u0e87-\u0e88] / "\u0e8a" / "\u0e8d" / [\u0e94-\u0e97] / [\u0e99-\u0e9f] / [\u0ea1-\u0ea3] / "\u0ea5" / "\u0ea7" / [\u0eaa-\u0eab] / [\u0ead-\u0eb9] / [\u0ebb-\u0ebd] / [\u0ec0-\u0ec4] / "\u0ec6" / [\u0ec8-\u0ecd] / [\u0ed0-\u0ed9] / [\u0edc-\u0edf] / "\u0f00" / [\u0f18-\u0f19] / [\u0f20-\u0f29] / "\u0f35" / "\u0f37" / "\u0f39" / [\u0f3e-\u0f47] / [\u0f49-\u0f6c] / [\u0f71-\u0f84] / [\u0f86-\u0f97] / [\u0f99-\u0fbc] / "\u0fc6" / [\u1000-\u1049] / [\u1050-\u109d] / [\u10a0-\u10c5] / "\u10c7" / "\u10cd" / [\u10d0-\u10fa] / [\u10fc-\u1248] / [\u124a-\u124d] / [\u1250-\u1256] / "\u1258" / [\u125a-\u125d] / [\u1260-\u1288] / [\u128a-\u128d] / [\u1290-\u12b0] / [\u12b2-\u12b5] / [\u12b8-\u12be] / "\u12c0" / [\u12c2-\u12c5] / [\u12c8-\u12d6] / [\u12d8-\u1310] / [\u1312-\u1315] / [\u1318-\u135a] / [\u135d-\u135f] / [\u1369-\u1371] / [\u1380-\u138f] / [\u13a0-\u13f5] / [\u13f8-\u13fd] / [\u1401-\u166c] / [\u166f-\u167f] / [\u1681-\u169a] / [\u16a0-\u16ea] / [\u16ee-\u16f8] / [\u1700-\u170c] / [\u170e-\u1714] / [\u1720-\u1734] / [\u1740-\u1753] / [\u1760-\u176c] / [\u176e-\u1770] / [\u1772-\u1773] / [\u1780-\u17d3] / "\u17d7" / [\u17dc-\u17dd] / [\u17e0-\u17e9] / [\u180b-\u180d] / [\u1810-\u1819] / [\u1820-\u1877] / [\u1880-\u18aa] / [\u18b0-\u18f5] / [\u1900-\u191e] / [\u1920-\u192b] / [\u1930-\u193b] / [\u1946-\u196d] / [\u1970-\u1974] / [\u1980-\u19ab] / [\u19b0-\u19c9] / [\u19d0-\u19da] / [\u1a00-\u1a1b] / [\u1a20-\u1a5e] / [\u1a60-\u1a7c] / [\u1a7f-\u1a89] / [\u1a90-\u1a99] / "\u1aa7" / [\u1ab0-\u1abd] / [\u1b00-\u1b4b] / [\u1b50-\u1b59] / [\u1b6b-\u1b73] / [\u1b80-\u1bf3] / [\u1c00-\u1c37] / [\u1c40-\u1c49] / [\u1c4d-\u1c7d] / [\u1cd0-\u1cd2] / [\u1cd4-\u1cf6] / [\u1cf8-\u1cf9] / [\u1d00-\u1df5] / [\u1dfc-\u1f15] / [\u1f18-\u1f1d] / [\u1f20-\u1f45] / [\u1f48-\u1f4d] / [\u1f50-\u1f57] / "\u1f59" / "\u1f5b" / "\u1f5d" / [\u1f5f-\u1f7d] / [\u1f80-\u1fb4] / [\u1fb6-\u1fbc] / "\u1fbe" / [\u1fc2-\u1fc4] / [\u1fc6-\u1fcc] / [\u1fd0-\u1fd3] / [\u1fd6-\u1fdb] / [\u1fe0-\u1fec] / [\u1ff2-\u1ff4] / [\u1ff6-\u1ffc] / [\u203f-\u2040] / "\u2054" / "\u2071" / "\u207f" / [\u2090-\u209c] / [\u20d0-\u20dc] / "\u20e1" / [\u20e5-\u20f0] / "\u2102" / "\u2107" / [\u210a-\u2113] / "\u2115" / [\u2118-\u211d] / "\u2124" / "\u2126" / "\u2128" / [\u212a-\u2139] / [\u213c-\u213f] / [\u2145-\u2149] / "\u214e" / [\u2160-\u2188] / [\u2c00-\u2c2e] / [\u2c30-\u2c5e] / [\u2c60-\u2ce4] / [\u2ceb-\u2cf3] / [\u2d00-\u2d25] / "\u2d27" / "\u2d2d" / [\u2d30-\u2d67] / "\u2d6f" / [\u2d7f-\u2d96] / [\u2da0-\u2da6] / [\u2da8-\u2dae] / [\u2db0-\u2db6] / [\u2db8-\u2dbe] / [\u2dc0-\u2dc6] / [\u2dc8-\u2dce] / [\u2dd0-\u2dd6] / [\u2dd8-\u2dde] / [\u2de0-\u2dff] / [\u3005-\u3007] / [\u3021-\u302f] / [\u3031-\u3035] / [\u3038-\u303c] / [\u3041-\u3096] / [\u3099-\u309f] / [\u30a1-\u30fa] / [\u30fc-\u30ff] / [\u3105-\u312d] / [\u3131-\u318e] / [\u31a0-\u31ba] / [\u31f0-\u31ff] / [\u3400-\u4db5] / [\u4e00-\u9fd5] / [\ua000-\ua48c] / [\ua4d0-\ua4fd] / [\ua500-\ua60c] / [\ua610-\ua62b] / [\ua640-\ua66f] / [\ua674-\ua67d] / [\ua67f-\ua6f1] / [\ua717-\ua71f] / [\ua722-\ua788] / [\ua78b-\ua7ad] / [\ua7b0-\ua7b7] / [\ua7f7-\ua827] / [\ua840-\ua873] / [\ua880-\ua8c4] / [\ua8d0-\ua8d9] / [\ua8e0-\ua8f7] / "\ua8fb" / "\ua8fd" / [\ua900-\ua92d] / [\ua930-\ua953] / [\ua960-\ua97c] / [\ua980-\ua9c0] / [\ua9cf-\ua9d9] / [\ua9e0-\ua9fe] / [\uaa00-\uaa36] / [\uaa40-\uaa4d] / [\uaa50-\uaa59] / [\uaa60-\uaa76] / [\uaa7a-\uaac2] / [\uaadb-\uaadd] / [\uaae0-\uaaef] / [\uaaf2-\uaaf6] / [\uab01-\uab06] / [\uab09-\uab0e] / [\uab11-\uab16] / [\uab20-\uab26] / [\uab28-\uab2e] / [\uab30-\uab5a] / [\uab5c-\uab65] / [\uab70-\uabea] / [\uabec-\uabed] / [\uabf0-\uabf9] / [\uac00-\ud7a3] / [\ud7b0-\ud7c6] / [\ud7cb-\ud7fb] / [\uf900-\ufa6d] / [\ufa70-\ufad9] / [\ufb00-\ufb06] / [\ufb13-\ufb17] / [\ufb1d-\ufb28] / [\ufb2a-\ufb36] / [\ufb38-\ufb3c] / "\ufb3e" / [\ufb40-\ufb41] / [\ufb43-\ufb44] / [\ufb46-\ufbb1] / [\ufbd3-\ufd3d] / [\ufd50-\ufd8f] / [\ufd92-\ufdc7] / [\ufdf0-\ufdfb] / [\ufe00-\ufe0f] / [\ufe20-\ufe2f] / [\ufe33-\ufe34] / [\ufe4d-\ufe4f] / [\ufe70-\ufe74] / [\ufe76-\ufefc] / [\uff10-\uff19] / [\uff21-\uff3a] / "\uff3f" / [\uff41-\uff5a] / [\uff66-\uffbe] / [\uffc2-\uffc7] / [\uffca-\uffcf] / [\uffd2-\uffd7] / [\uffda-\uffdc] / ("\u0001" [\u0000-\u000b]) / ("\u0001" [\u000d-\u0026]) / ("\u0001" [\u0028-\u003a]) / ("\u0001" [\u003c-\u003d]) / ("\u0001" [\u003f-\u004d]) / ("\u0001" [\u0050-\u005d]) / ("\u0001" [\u0080-\u00fa]) / ("\u0001" [\u0140-\u0174]) / ("\u0001\u01fd") / ("\u0001" [\u0280-\u029c]) / ("\u0001" [\u02a0-\u02d0]) / ("\u0001\u02e0") / ("\u0001" [\u0300-\u031f]) / ("\u0001" [\u0330-\u034a]) / ("\u0001" [\u0350-\u037a]) / ("\u0001" [\u0380-\u039d]) / ("\u0001" [\u03a0-\u03c3]) / ("\u0001" [\u03c8-\u03cf]) / ("\u0001" [\u03d1-\u03d5]) / ("\u0001" [\u0400-\u049d]) / ("\u0001" [\u04a0-\u04a9]) / ("\u0001" [\u0500-\u0527]) / ("\u0001" [\u0530-\u0563]) / ("\u0001" [\u0600-\u0736]) / ("\u0001" [\u0740-\u0755]) / ("\u0001" [\u0760-\u0767]) / ("\u0001" [\u0800-\u0805]) / ("\u0001\u0808") / ("\u0001" [\u080a-\u0835]) / ("\u0001" [\u0837-\u0838]) / ("\u0001\u083c") / ("\u0001" [\u083f-\u0855]) / ("\u0001" [\u0860-\u0876]) / ("\u0001" [\u0880-\u089e]) / ("\u0001" [\u08e0-\u08f2]) / ("\u0001" [\u08f4-\u08f5]) / ("\u0001" [\u0900-\u0915]) / ("\u0001" [\u0920-\u0939]) / ("\u0001" [\u0980-\u09b7]) / ("\u0001" [\u09be-\u09bf]) / ("\u0001" [\u0a00-\u0a03]) / ("\u0001" [\u0a05-\u0a06]) / ("\u0001" [\u0a0c-\u0a13]) / ("\u0001" [\u0a15-\u0a17]) / ("\u0001" [\u0a19-\u0a33]) / ("\u0001" [\u0a38-\u0a3a]) / ("\u0001\u0a3f") / ("\u0001" [\u0a60-\u0a7c]) / ("\u0001" [\u0a80-\u0a9c]) / ("\u0001" [\u0ac0-\u0ac7]) / ("\u0001" [\u0ac9-\u0ae6]) / ("\u0001" [\u0b00-\u0b35]) / ("\u0001" [\u0b40-\u0b55]) / ("\u0001" [\u0b60-\u0b72]) / ("\u0001" [\u0b80-\u0b91]) / ("\u0001" [\u0c00-\u0c48]) / ("\u0001" [\u0c80-\u0cb2]) / ("\u0001" [\u0cc0-\u0cf2]) / ("\u0001" [\u1000-\u1046]) / ("\u0001" [\u1066-\u106f]) / ("\u0001" [\u107f-\u10ba]) / ("\u0001" [\u10d0-\u10e8]) / ("\u0001" [\u10f0-\u10f9]) / ("\u0001" [\u1100-\u1134]) / ("\u0001" [\u1136-\u113f]) / ("\u0001" [\u1150-\u1173]) / ("\u0001\u1176") / ("\u0001" [\u1180-\u11c4]) / ("\u0001" [\u11ca-\u11cc]) / ("\u0001" [\u11d0-\u11da]) / ("\u0001\u11dc") / ("\u0001" [\u1200-\u1211]) / ("\u0001" [\u1213-\u1237]) / ("\u0001" [\u1280-\u1286]) / ("\u0001\u1288") / ("\u0001" [\u128a-\u128d]) / ("\u0001" [\u128f-\u129d]) / ("\u0001" [\u129f-\u12a8]) / ("\u0001" [\u12b0-\u12ea]) / ("\u0001" [\u12f0-\u12f9]) / ("\u0001" [\u1300-\u1303]) / ("\u0001" [\u1305-\u130c]) / ("\u0001" [\u130f-\u1310]) / ("\u0001" [\u1313-\u1328]) / ("\u0001" [\u132a-\u1330]) / ("\u0001" [\u1332-\u1333]) / ("\u0001" [\u1335-\u1339]) / ("\u0001" [\u133c-\u1344]) / ("\u0001" [\u1347-\u1348]) / ("\u0001" [\u134b-\u134d]) / ("\u0001\u1350") / ("\u0001\u1357") / ("\u0001" [\u135d-\u1363]) / ("\u0001" [\u1366-\u136c]) / ("\u0001" [\u1370-\u1374]) / ("\u0001" [\u1480-\u14c5]) / ("\u0001\u14c7") / ("\u0001" [\u14d0-\u14d9]) / ("\u0001" [\u1580-\u15b5]) / ("\u0001" [\u15b8-\u15c0]) / ("\u0001" [\u15d8-\u15dd]) / ("\u0001" [\u1600-\u1640]) / ("\u0001\u1644") / ("\u0001" [\u1650-\u1659]) / ("\u0001" [\u1680-\u16b7]) / ("\u0001" [\u16c0-\u16c9]) / ("\u0001" [\u1700-\u1719]) / ("\u0001" [\u171d-\u172b]) / ("\u0001" [\u1730-\u1739]) / ("\u0001" [\u18a0-\u18e9]) / ("\u0001\u18ff") / ("\u0001" [\u1ac0-\u1af8]) / ("\u0001" [\u2000-\u2399]) / ("\u0001" [\u2400-\u246e]) / ("\u0001" [\u2480-\u2543]) / ("\u0001" [\u3000-\u342e]) / ("\u0001" [\u4400-\u4646]) / ("\u0001" [\u6800-\u6a38]) / ("\u0001" [\u6a40-\u6a5e]) / ("\u0001" [\u6a60-\u6a69]) / ("\u0001" [\u6ad0-\u6aed]) / ("\u0001" [\u6af0-\u6af4]) / ("\u0001" [\u6b00-\u6b36]) / ("\u0001" [\u6b40-\u6b43]) / ("\u0001" [\u6b50-\u6b59]) / ("\u0001" [\u6b63-\u6b77]) / ("\u0001" [\u6b7d-\u6b8f]) / ("\u0001" [\u6f00-\u6f44]) / ("\u0001" [\u6f50-\u6f7e]) / ("\u0001" [\u6f8f-\u6f9f]) / ("\u0001" [\ub000-\ub001]) / ("\u0001" [\ubc00-\ubc6a]) / ("\u0001" [\ubc70-\ubc7c]) / ("\u0001" [\ubc80-\ubc88]) / ("\u0001" [\ubc90-\ubc99]) / ("\u0001" [\ubc9d-\ubc9e]) / ("\u0001" [\ud165-\ud169]) / ("\u0001" [\ud16d-\ud172]) / ("\u0001" [\ud17b-\ud182]) / ("\u0001" [\ud185-\ud18b]) / ("\u0001" [\ud1aa-\ud1ad]) / ("\u0001" [\ud242-\ud244]) / ("\u0001" [\ud400-\ud454]) / ("\u0001" [\ud456-\ud49c]) / ("\u0001" [\ud49e-\ud49f]) / ("\u0001\ud4a2") / ("\u0001" [\ud4a5-\ud4a6]) / ("\u0001" [\ud4a9-\ud4ac]) / ("\u0001" [\ud4ae-\ud4b9]) / ("\u0001\ud4bb") / ("\u0001" [\ud4bd-\ud4c3]) / ("\u0001" [\ud4c5-\ud505]) / ("\u0001" [\ud507-\ud50a]) / ("\u0001" [\ud50d-\ud514]) / ("\u0001" [\ud516-\ud51c]) / ("\u0001" [\ud51e-\ud539]) / ("\u0001" [\ud53b-\ud53e]) / ("\u0001" [\ud540-\ud544]) / ("\u0001\ud546") / ("\u0001" [\ud54a-\ud550]) / ("\u0001" [\ud552-\ud6a5]) / ("\u0001" [\ud6a8-\ud6c0]) / ("\u0001" [\ud6c2-\ud6da]) / ("\u0001" [\ud6dc-\ud6fa]) / ("\u0001" [\ud6fc-\ud714]) / ("\u0001" [\ud716-\ud734]) / ("\u0001" [\ud736-\ud74e]) / ("\u0001" [\ud750-\ud76e]) / ("\u0001" [\ud770-\ud788]) / ("\u0001" [\ud78a-\ud7a8]) / ("\u0001" [\ud7aa-\ud7c2]) / ("\u0001" [\ud7c4-\ud7cb]) / ("\u0001" [\ud7ce-\ud7ff]) / ("\u0001" [\ue800-\ue8c4]) / ("\u0001" [\ue8d0-\ue8d6]) / ("\u0001" [\uee00-\uee03]) / ("\u0001" [\uee05-\uee1f]) / ("\u0001" [\uee21-\uee22]) / ("\u0001\uee24") / ("\u0001\uee27") / ("\u0001" [\uee29-\uee32]) / ("\u0001" [\uee34-\uee37]) / ("\u0001\uee39") / ("\u0001\uee3b") / ("\u0001\uee42") / ("\u0001\uee47") / ("\u0001\uee49") / ("\u0001\uee4b") / ("\u0001" [\uee4d-\uee4f]) / ("\u0001" [\uee51-\uee52]) / ("\u0001\uee54") / ("\u0001\uee57") / ("\u0001\uee59") / ("\u0001\uee5b") / ("\u0001\uee5d") / ("\u0001\uee5f") / ("\u0001" [\uee61-\uee62]) / ("\u0001\uee64") / ("\u0001" [\uee67-\uee6a]) / ("\u0001" [\uee6c-\uee72]) / ("\u0001" [\uee74-\uee77]) / ("\u0001" [\uee79-\uee7c]) / ("\u0001\uee7e") / ("\u0001" [\uee80-\uee89]) / ("\u0001" [\uee8b-\uee9b]) / ("\u0001" [\ueea1-\ueea3]) / ("\u0001" [\ueea5-\ueea9]) / ("\u0001" [\ueeab-\ueebb]) / ("\u0002" [\u0000-\ua6d6]) / ("\u0002" [\ua700-\ub734]) / ("\u0002" [\ub740-\ub81d]) / ("\u0002" [\ub820-\ucea1]) / ("\u0002" [\uf800-\ufa1d]) / ("\u000e" [\u0100-\u01ef])

version(GenerateGrammar):

void main()
{
    import pegged.grammar;
    asModule("es6.grammar", "source/es6/grammar",`
    ES6:
        Module                  < ModuleBody eoi
        ModuleBody              < ModuleItemList
        ModuleItemList          < (ImportDeclaration / ExportDeclaration / StatementListItem)+
        ImportDeclaration       < "import" ((ImportClause FromClause) / ModuleSpecifier) ";"
        ImportClause            < NameSpaceImport / NamedImports / (ImportedDefaultBinding "," NameSpaceImport) / (ImportedDefaultBinding "," NamedImports) / ImportedDefaultBinding
        ImportedDefaultBinding  < ImportedBinding
        NameSpaceImport         < "*" "as" ImportedBinding
        NamedImports            < CurlyBrackets((ImportsList (",")?)?)
        FromClause              < "from" ModuleSpecifier
        ImportsList             < ImportSpecifier ("," ImportSpecifier)*
        ImportSpecifier         < (IdentifierName "as" ImportedBinding) / ImportedBinding
        ModuleSpecifier         < StringLiteral
        ImportedBinding         < BindingIdentifier
        ExportDeclaration       <   / ("export" "*" FromClause ";")
                                    / ("export" ExportClause FromClause ";")
                                    / ("export" ExportClause ";")
                                    / ("export" "default" HoistableDeclarationDefault)
                                    / ("export" "default" ClassDeclarationDefault)
                                    / ("export" "default" !("function" / "class") AssignmentExpressionIn ";")
                                    / ("export" VariableStatement)
                                    / ("export" Declaration)

        ExportClause                        < CurlyBrackets((ExportsList (",")?)?)
        ExportsList                         < ExportSpecifier ("," ExportSpecifier)*
        ExportSpecifier                     < IdentifierName ("as" IdentifierName)?
        SourceCharacter                     <- [\u0000-\uFFFC]
        InputElementDiv                     <- WhiteSpace / LineTerminator / Comment / CommonToken / DivPunctuator / RightBracePunctuator
        InputElementRegExp                  <- WhiteSpace / LineTerminator / Comment / CommonToken / RightBracePunctuator / RegularExpressionLiteral
        InputElementRegExpOrTemplateTail    <- WhiteSpace / LineTerminator / Comment / CommonToken / RegularExpressionLiteral / TemplateSubstitutionTail
        InputElementTemplateTail            <- WhiteSpace / LineTerminator / Comment / CommonToken / DivPunctuator / TemplateSubstitutionTail

        WhiteSpace                      <- "\u0009" / "\u000B" / "\u000C" / "\u0020" / "\u00A0" / "\uFEFF" / [\u02B0-\u02FF]
        LineTerminator                  <- "\u000A" / "\u000D" / "\u2028" / "\u2029"
        LineTerminatorSequence          <- "\u000A" / ("\u000D" !"\u000A") / "\u2028" / "\u2029" / ("\u000D\u000A")
        Spacing                         <~ (:WhiteSpace / :LineTerminatorSequence / Comment)*
        Spaces                          <~ (:WhiteSpace / Comment)*

        Comment                         <~ MultiLineComment / SingleLineComment
        MultiLineComment                <~ :"/*" (!"*/" SourceCharacter)* :"*/"
        SingleLineComment               <- :"//" SingleLineCommentChars? eol
        SingleLineCommentChars          <- SingleLineCommentChar+
        SingleLineCommentChar           <- !LineTerminator SourceCharacter

        CommonToken                     <- IdentifierName / Punctuator / NumericLiteral / StringLiteral / Template
        IdentifierName                  <~ IdentifierStart IdentifierPart*
        IdentifierStart                 <~ UnicodeIDStart / "$" / "_" / ("\\" UnicodeEscapeSequence)
        IdentifierPart                  <~ UnicodeIDContinue / "$" / "_" / ("\\" UnicodeEscapeSequence) / "\u200C" / "\u200D"
        UnicodeIDStart                  <~ ID_Start / Other_ID_Start

        ID_Start                        <- [\u0041-\u005a] / [\u0061-\u007a] / "\u00aa" / "\u00b5" / "\u00ba" / [\u00c0-\u00d6] / [\u00d8-\u00f6]
        Other_ID_Start                  <- "\u2118" / "\u212e" / [\u309b-\u309c]
        UnicodeIDContinue               <- ID_Continue / Other_ID_Continue / Other_ID_Start
        ID_Continue                     <- [\u0030-\u0039] / [\u0041-\u005a] / "\u005f" / [\u0061-\u007a] / "\u00aa" / "\u00b5" / "\u00b7" / "\u00ba" / [\u00c0-\u00d6] / [\u00d8-\u00f6]
        Other_ID_Continue               <- "\u00b7" / "\u0387" / [\u1369-\u1371] / "\u19da"

        ReservedWord                    <- Keyword / FutureReservedWord / NullLiteral / BooleanLiteral
        Keyword                         <- "break" / "do" / "in" / "typeof" / "case" / "else" / "instanceof" / "var" / "catch" / "export" / "new" / "void" / "class" / "extends" / "return" / "while" / "const" / "finally" / "super" / "with" / "continue" / "for" / "switch" / "yield" / "debugger" / "function" / "this" / "default" / "if" / "throw" / "delete" / "import" / "try"
        FutureReservedWord              <- "enum" / "await"
        Punctuator                      <- "{" / "}" / "(" / ")" / "[" / "]" / "." / ";" / "," / "<=" / ">=" / "<" / ">" / "===" / "!==" / "==" / "!=" / "++" / "--" / "*=" / "*" / "%=" / "%" / "+=" / "+" / "-=" / "-" / "<<=" / "<<" / ">>>=" / ">>>" / ">>=" / ">>" / "&&" / "||" / "^=" / "^" / "!" / "~" / "&=" / "&" / "|=" / "|" / "?" / ":" / "=>" / "="
        DivPunctuator                   <- "/=" / "/"
        RightBracePunctuator            <- "}"
        NullLiteral                     <- "null"
        BooleanLiteral                  <- "true" / "false"
        NumericLiteral                  <- HexIntegerLiteral / DecimalLiteral / BinaryIntegerLiteral / OctalIntegerLiteral
        DecimalLiteral                  <~ (DecimalIntegerLiteral ("." DecimalDigits?)? ExponentPart?) / ("." DecimalDigits ExponentPart?)
        DecimalIntegerLiteral           <- "0" / (NonZeroDigit DecimalDigits?)
        DecimalDigits                   <~ DecimalDigit+
        DecimalDigit                    <- [0-9]
        NonZeroDigit                    <- [1-9]
        ExponentPart                    <- ExponentIndicator SignedInteger
        ExponentIndicator               <- "e" / "E"
        SignedInteger                   <- ("+" / "-")? DecimalDigits
        BinaryIntegerLiteral            <~ ("0b" / "0B") BinaryDigits
        BinaryDigits                    <~ BinaryDigit+
        BinaryDigit                     <- "0" / "1"
        OctalIntegerLiteral             <~ ("0o" / "0O") OctalDigits
        OctalDigits                     <- OctalDigit+
        OctalDigit                      <- [0-7]
        HexIntegerLiteral               <~ ("0x" / "0X") HexDigits
        HexDigits                       <- HexDigit+
        HexDigit                        <- [0-9a-fA-F]

        StringLiteral                   <~ ("\"" DoubleStringCharacters? "\"") / ("'" SingleStringCharacters? "'")
        DoubleStringCharacters          <- DoubleStringCharacter+
        SingleStringCharacters          <- SingleStringCharacter+
        DoubleStringCharacter           <- (!("\"" / "\\" / LineTerminator) SourceCharacter) / ("\\" EscapeSequence) / LineContinuation
        SingleStringCharacter           <- (!("\'" / "\\" / LineTerminator) SourceCharacter) / ("\\" EscapeSequence) / LineContinuation
        LineContinuation                <- "\\" LineTerminatorSequence
        EscapeSequence                  <- (CharacterEscapeSequence / ("0" !DecimalDigit)) / HexEscapeSequence / UnicodeEscapeSequence
        CharacterEscapeSequence         <- SingleEscapeCharacter / NonEscapeCharacter
        SingleEscapeCharacter           <- "'" / "\"" / "\\" / "b" / "f" / "n" / "r" / "t" / "v"
        NonEscapeCharacter              <- !(EscapeCharacter / LineTerminator) SourceCharacter
        EscapeCharacter                 <- SingleEscapeCharacter / DecimalDigit / "x" / "u"
        HexEscapeSequence               <- "x" HexDigit HexDigit
        UnicodeEscapeSequence           <- ("u" Hex4Digits) / "u{" HexDigits "}"
        Hex4Digits                      <- HexDigit HexDigit HexDigit HexDigit

        RegularExpressionLiteral        <- "/" RegularExpressionBody "/" RegularExpressionFlags
        RegularExpressionBody           <- RegularExpressionFirstChar RegularExpressionChars
        RegularExpressionChars          <- RegularExpressionChar*

        RegularExpressionFirstChar          <- (!("*" / "\\" / "/" / "[") RegularExpressionNonTerminator) / RegularExpressionBackslashSequence / RegularExpressionClass
        RegularExpressionChar               <- (!("\\" / "/" / "[") RegularExpressionNonTerminator) / RegularExpressionBackslashSequence / RegularExpressionClass
        RegularExpressionBackslashSequence  <- "\\" RegularExpressionNonTerminator
        RegularExpressionNonTerminator      <- !LineTerminator SourceCharacter
        RegularExpressionClass              <- :"[" RegularExpressionClassChars :"]"
        RegularExpressionClassChars         <- RegularExpressionClassChar*
        RegularExpressionClassChar          <- (!("]" / "\\") RegularExpressionNonTerminator) / RegularExpressionBackslashSequence
        RegularExpressionFlags              <- IdentifierPart*

        Template                            <- NoSubstitutionTemplate / TemplateHead
        NoSubstitutionTemplate              <- :"\u0060" TemplateCharacters? :"\u0060"
        TemplateHead                        <- :"\u0060" TemplateCharacters? :"${"
        TemplateSubstitutionTail            <- TemplateMiddle / TemplateTail
        TemplateMiddle                      <- :"}" TemplateCharacters? :"${"
        TemplateTail                        <- :"}" TemplateCharacters? :"\u0060"
        TemplateCharacters                  <- TemplateCharacter+
        TemplateCharacter                   <- ("$" !"{") / ("\\" EscapeSequence) / LineContinuation / LineTerminatorSequence / (!("\u0060" / "\\" / "$" / LineTerminator) SourceCharacter)

        IdentifierReference                 < Identifier / "yield"
        IdentifierReferenceYield            < Identifier
        BindingIdentifier                   < Identifier / "yield"
        BindingIdentifierYield              < Identifier
        LabelIdentifier                     < Identifier / "yield"
        LabelIdentifierYield                < Identifier
        Identifier                          <- !(ReservedWord (WhiteSpace / LineTerminatorSequence / Comment / ";" / ":")) IdentifierName
        ThisKeyword                         <- ("this" !IdentifierPart)

        PrimaryExpression                                       < ThisKeyword / Literal / ArrayLiteral / ObjectLiteral / GeneratorExpression / ClassExpression / FunctionExpression / RegularExpressionLiteral / TemplateLiteral / CoverParenthesizedExpressionAndArrowParameterList / IdentifierReference
        PrimaryExpressionYield                                  < ThisKeyword / Literal / ArrayLiteralYield / ObjectLiteralYield / GeneratorExpression / ClassExpressionYield / FunctionExpression / RegularExpressionLiteral / TemplateLiteralYield / CoverParenthesizedExpressionAndArrowParameterListYield / IdentifierReferenceYield
        CoverParenthesizedExpressionAndArrowParameterList       < Parentheses(ExpressionIn "," "..." BindingIdentifier) / Parentheses("..." BindingIdentifier) / Parentheses(ExpressionIn?)
        CoverParenthesizedExpressionAndArrowParameterListYield  < Parentheses(ExpressionInYield "," "..." BindingIdentifierYield) / Parentheses("..." BindingIdentifierYield) / Parentheses(ExpressionInYield?)

        ParenthesizedExpression         < Parentheses(ExpressionIn)
        ParenthesizedExpressionYield    < Parentheses(ExpressionInYield)
        Literal                         <- ((NullLiteral / BooleanLiteral / NumericLiteral) !(IdentifierStart / IdentifierPart)) / StringLiteral
        ArrayLiteral                    < SquareBrackets(ElementList? Elision?)
        ArrayLiteralYield               < SquareBrackets(ElementListYield? Elision?)
        ElementList                     < Elision? (AssignmentExpressionIn / SpreadElement) (:"," Elision? (AssignmentExpressionIn / SpreadElement))*
        ElementListYield                < Elision? (AssignmentExpressionInYield / SpreadElementYield) (:"," Elision? (AssignmentExpressionInYield / SpreadElementYield))*
        Elision                         < ","+
        SpreadElement                   < :"..." AssignmentExpressionIn
        SpreadElementYield              < :"..." AssignmentExpressionInYield
        ObjectLiteral                   < CurlyBrackets((PropertyDefinitionList :","?)?)
        ObjectLiteralYield              < CurlyBrackets((PropertyDefinitionListYield :","?)?)
        PropertyDefinitionList          < PropertyDefinition ("," PropertyDefinition)*
        PropertyDefinitionListYield     < PropertyDefinitionYield ("," PropertyDefinitionYield)*
        PropertyDefinition              < MethodDefinition / CoverInitializedName / (PropertyName ":" AssignmentExpressionIn) / IdentifierReference
        PropertyDefinitionYield         < MethodDefinitionYield / CoverInitializedNameYield / (PropertyNameYield ":" AssignmentExpressionInYield) / IdentifierReferenceYield
        PropertyName                    < ComputedPropertyName / LiteralPropertyName
        PropertyNameYield               < ComputedPropertyNameYield / LiteralPropertyName
        LiteralPropertyName             < NumericLiteral / StringLiteral / IdentifierName
        ComputedPropertyName            < SquareBrackets(AssignmentExpressionIn)
        ComputedPropertyNameYield       < SquareBrackets(AssignmentExpressionInYield)
        CoverInitializedName            < IdentifierReference InitializerIn
        CoverInitializedNameYield       < IdentifierReferenceYield InitializerInYield
        Initializer                     < :"=" AssignmentExpression
        InitializerIn                   < :"=" AssignmentExpressionIn
        InitializerYield                < :"=" AssignmentExpressionYield
        InitializerInYield              < :"=" AssignmentExpressionInYield
        TemplateLiteral                 < NoSubstitutionTemplate / (TemplateHead ExpressionIn TemplateSpans)
        TemplateLiteralYield            < NoSubstitutionTemplate / (TemplateHead ExpressionInYield TemplateSpansYield)
        TemplateSpans                   < TemplateMiddleList? TemplateTail
        TemplateSpansYield              < TemplateMiddleListYield? TemplateTail
        TemplateMiddleList              < TemplateMiddleList? TemplateMiddle ExpressionIn
        TemplateMiddleListYield         < TemplateMiddleListYield? TemplateMiddle ExpressionInYield
        FieldAccessor                   < "." IdentifierName
        MemberExpression                < ((SuperProperty / MetaProperty / PrimaryExpression) / (NewKeyword ((SuperProperty / MetaProperty / PrimaryExpression) (SquareBrackets(ExpressionIn) / (FieldAccessor) / TemplateLiteral)*) Arguments)) (SquareBrackets(ExpressionIn) / (FieldAccessor) / TemplateLiteral)*
        MemberExpressionYield           < ((SuperPropertyYield / MetaProperty / PrimaryExpressionYield) / (NewKeyword ((SuperPropertyYield / MetaProperty / PrimaryExpressionYield) (SquareBrackets(ExpressionInYield) / (FieldAccessor) / TemplateLiteralYield)*) ArgumentsYield)) (SquareBrackets(ExpressionInYield) / (FieldAccessor) / TemplateLiteralYield)*
        SuperProperty                   < (:"super" SquareBrackets(ExpressionIn)) / (:"super" FieldAccessor)
        SuperPropertyYield              < (:"super" SquareBrackets(ExpressionInYield)) / (:"super" FieldAccessor)
        MetaProperty                    < NewTarget
        NewKeyword						<- "new" !(IdentifierStart / IdentifierPart)
        NewTarget                       < :"new" "." :"target"
        NewExpression                   < MemberExpression / ((NewKeyword WhiteSpace?)* MemberExpression)
        NewExpressionYield              < MemberExpressionYield / ((NewKeyword WhiteSpace?)* MemberExpressionYield)
        CallExpression                  < (SuperCall / (MemberExpression Arguments)) (SquareBrackets(ExpressionIn) / (FieldAccessor) / TemplateLiteral / Arguments)*
        CallExpressionYield             < (SuperCallYield / (MemberExpressionYield ArgumentsYield)) (SquareBrackets(ExpressionInYield) / (FieldAccessor) / TemplateLiteralYield / ArgumentsYield)*
        SuperCall                       < :"super" Arguments
        SuperCallYield                  < :"super" ArgumentsYield
        Arguments                       < Parentheses(ArgumentList?)
        ArgumentsYield                  < Parentheses(ArgumentListYield?)
        ArgumentList                    < "..."? AssignmentExpressionIn ("," ArgumentList)*
        ArgumentListYield               < "..."? AssignmentExpressionInYield ("," ArgumentListYield)*
        LeftHandSideExpression          < CallExpression / NewExpression
        LeftHandSideExpressionYield     < CallExpressionYield / NewExpressionYield
        PostfixExpression               < LeftHandSideExpression (Spaces (PostfixOperator))?
        PostfixExpressionYield          < LeftHandSideExpressionYield (Spaces (PostfixOperator))?
        PostfixOperator                 < "++" / "--"
        PrefixExpression                <- ((("delete" / "void" / "typeof") !(IdentifierStart / IdentifierPart)) / "++" / "--" / "+" / "-" / "~" / "!")
        UnaryExpression                 < PrefixExpression* PostfixExpression
        UnaryExpressionYield            < PrefixExpression* PostfixExpressionYield
        RightHandSideExpression         < BinaryExpression / UnaryExpression
        RightHandSideExpressionIn       < BinaryExpressionIn / UnaryExpression
        RightHandSideExpressionYield    < BinaryExpressionYield / UnaryExpressionYield
        RightHandSideExpressionInYield  < BinaryExpressionInYield / UnaryExpressionYield
        BinaryExpression                < UnaryExpression (ExpressionOperator UnaryExpression)+
        BinaryExpressionIn              < UnaryExpression (ExpressionOperatorIn UnaryExpression)+
        BinaryExpressionYield           < UnaryExpressionYield (ExpressionOperator UnaryExpressionYield)+
        BinaryExpressionInYield         < UnaryExpressionYield (ExpressionOperatorIn UnaryExpressionYield)+
        ExpressionOperator              < LogicalOperator / BitwiseOperator / EqualityOperator / ShiftOperator / RelationalOperator / AdditiveOperator / MultiplicativeOperator
        ExpressionOperatorIn            < LogicalOperator / BitwiseOperator / EqualityOperator / ShiftOperator / RelationalOperatorIn / AdditiveOperator / MultiplicativeOperator
        LogicalOperator                 < "||" / "&&"
        BitwiseOperator                 < "|" / "^" / "&"
        EqualityOperator                < "===" / "!==" / "==" / "!="
        RelationalOperator              < "<=" / ">=" / "<" / ">" / "instanceof"
        RelationalOperatorIn            < "<=" / ">=" / "<" / ">" / "instanceof" / "in"
        ShiftOperator                   < "<<" / ">>>" / ">>"
        AdditiveOperator                <- "+" / "-"
        MultiplicativeOperator          < "*" / "/" / "%"
        ConditionalExpression           < RightHandSideExpression ("?" AssignmentExpressionIn ":" AssignmentExpression)?
        ConditionalExpressionIn         < RightHandSideExpressionIn ("?" AssignmentExpressionIn ":" AssignmentExpressionIn)?
        ConditionalExpressionYield      < RightHandSideExpressionYield ("?" AssignmentExpressionInYield ":" AssignmentExpressionYield)?
        ConditionalExpressionInYield    < RightHandSideExpressionInYield ("?" AssignmentExpressionInYield ":" AssignmentExpressionInYield)?
        AssignmentExpression            < ((JSXElement / ArrowFunction / ConditionalExpression) !AssignmentOperator) / ((LeftHandSideExpression AssignmentOperator)* (JSXElement / ArrowFunction / ConditionalExpression))
        AssignmentExpressionIn          < ((JSXElement / ArrowFunctionIn / ConditionalExpressionIn) !AssignmentOperator) / ((LeftHandSideExpression AssignmentOperator)* (JSXElement / ArrowFunctionIn / ConditionalExpressionIn))
        AssignmentExpressionYield       < (YieldExpression / ArrowFunctionYield / ConditionalExpressionYield) / ((LeftHandSideExpressionYield AssignmentOperator)* (YieldExpression / ArrowFunctionYield / ConditionalExpressionYield))
        AssignmentExpressionInYield     < (YieldExpressionIn / ArrowFunctionInYield / ConditionalExpressionInYield) / ((LeftHandSideExpressionYield AssignmentOperator)* (YieldExpressionIn / ArrowFunctionInYield / ConditionalExpressionInYield))
        AssignmentOperator              < (!"==" "=") / "*=" / "/=" / "%=" / "+=" / "-=" / "<<=" / ">>=" / ">>>=" / "&=" / "^=" / "|="
        Comma                           < ","
        Expression                      < AssignmentExpression (Comma AssignmentExpression)*
        ExpressionIn                    < AssignmentExpressionIn (Comma AssignmentExpressionIn)*
        ExpressionYield                 < AssignmentExpressionYield (Comma AssignmentExpressionYield)*
        ExpressionInYield               < AssignmentExpressionInYield (Comma AssignmentExpressionInYield)*

        Statement                       < (BlockStatement / VariableStatement / EmptyStatement / IfStatement / BreakableStatement / ContinueStatement / BreakStatement / WithStatement / LabelledStatement / ThrowStatement / TryStatement / DebuggerStatement / ExpressionStatement) :";"?
        StatementYield                  < (BlockStatementYield / VariableStatementYield / EmptyStatement / IfStatementYield / BreakableStatementYield / ContinueStatementYield / BreakStatementYield / WithStatementYield / LabelledStatementYield / ThrowStatementYield / TryStatementYield / DebuggerStatement / ExpressionStatementYield) :";"?
        StatementReturn                 < (BlockStatementReturn / VariableStatement / EmptyStatement / IfStatementReturn / BreakableStatementReturn / ContinueStatement / BreakStatement / ReturnStatement / WithStatementReturn / LabelledStatementReturn / ThrowStatement / TryStatementReturn / DebuggerStatement / ExpressionStatement) :";"?
        StatementYieldReturn            < (BlockStatementYieldReturn / VariableStatementYield / EmptyStatement / IfStatementYieldReturn / BreakableStatementYieldReturn / ContinueStatementYield / BreakStatementYield / ReturnStatementYield / WithStatementYieldReturn / LabelledStatementYieldReturn / ThrowStatementYield / TryStatementYieldReturn / DebuggerStatement / ExpressionStatementYield) :";"?

        Declaration                         < HoistableDeclaration / ClassDeclaration / LexicalDeclarationIn
        DeclarationYield                    < HoistableDeclarationYield / ClassDeclarationYield / LexicalDeclarationInYield
        HoistableDeclaration                < GeneratorDeclaration / FunctionDeclaration
        HoistableDeclarationYield           < GeneratorDeclarationYield / FunctionDeclarationYield
        HoistableDeclarationDefault         < GeneratorDeclarationDefault / FunctionDeclarationDefault
        HoistableDeclarationYieldDefault    < GeneratorDeclarationYieldDefault / FunctionDeclarationYieldDefault

        BreakableStatement                  < IterationStatement / SwitchStatement
        BreakableStatementYield             < IterationStatementYield / SwitchStatementYield
        BreakableStatementReturn            < IterationStatementReturn / SwitchStatementReturn
        BreakableStatementYieldReturn       < IterationStatementYieldReturn / SwitchStatementYieldReturn

        BlockStatement                      < %Block
        BlockStatementYield                 < %BlockYield
        BlockStatementReturn                < %BlockReturn
        BlockStatementYieldReturn           < %BlockYieldReturn

        Block                               < CurlyBrackets(StatementList)
        BlockYield                          < CurlyBrackets(StatementListYield)
        BlockReturn                         < CurlyBrackets(StatementListReturn)
        BlockYieldReturn                    < CurlyBrackets(StatementListYieldReturn)

        StatementList                       < StatementListItem*
        StatementListYield                  < StatementListItemYield*
        StatementListReturn                 < StatementListItemReturn*
        StatementListYieldReturn            < StatementListItemYieldReturn*

        StatementListItem                   < Declaration / Statement
        StatementListItemYield              < DeclarationYield / StatementYield
        StatementListItemReturn             < Declaration / StatementReturn
        StatementListItemYieldReturn        < DeclarationYield / StatementYieldReturn

        LexicalDeclaration                  < LetOrConst BindingList :";"
        LexicalDeclarationIn                < LetOrConst BindingListIn :";"
        LexicalDeclarationYield             < LetOrConst BindingListYield :";"
        LexicalDeclarationInYield           < LetOrConst BindingListInYield :";"
        LetOrConst                          < ("let" / "const")

        BindingList                         < LexicalBinding (:"," LexicalBinding)*
        BindingListIn                       < LexicalBindingIn (:"," LexicalBindingIn)*
        BindingListYield                    < LexicalBindingYield (:"," LexicalBindingYield)*
        BindingListInYield                  < LexicalBindingInYield (:"," LexicalBindingInYield)*

        LexicalBinding                      < (BindingPatternYield Initializer) / (BindingIdentifier Initializer?)
        LexicalBindingIn                    < (BindingPatternYield InitializerIn) / (BindingIdentifier InitializerIn?)
        LexicalBindingYield                 < (BindingPatternYield InitializerYield) / (BindingIdentifierYield InitializerYield?)
        LexicalBindingInYield               < (BindingPatternYield InitializerInYield) / (BindingIdentifierYield InitializerInYield?)

        VariableStatement                   < :"var" VariableDeclarationListIn
        VariableStatementYield              < :"var" VariableDeclarationListInYield

        VariableDeclarationList             < VariableDeclaration (:"," VariableDeclaration)*
        VariableDeclarationListIn           < VariableDeclarationIn (:"," VariableDeclarationIn)*
        VariableDeclarationListYield        < VariableDeclarationYield (:"," VariableDeclarationYield)*
        VariableDeclarationListInYield      < VariableDeclarationInYield (:"," VariableDeclarationInYield)*

        VariableDeclaration             < (BindingPattern Initializer) / (BindingIdentifier Initializer?)
        VariableDeclarationIn           < (BindingPattern InitializerIn) / (BindingIdentifier InitializerIn?)
        VariableDeclarationYield        < (BindingPatternYield InitializerYield) / (BindingIdentifierYield InitializerYield?)
        VariableDeclarationInYield      < (BindingPatternYield InitializerInYield) / (BindingIdentifierYield InitializerInYield?)

        BindingPattern                  < ObjectBindingPattern / ArrayBindingPattern
        BindingPatternYield             < ObjectBindingPatternYield / ArrayBindingPatternYield
        ObjectBindingPattern            < CurlyBrackets((BindingPropertyList (:",")?)?)
        ObjectBindingPatternYield       < CurlyBrackets((BindingPropertyListYield (:",")?)?)

        ArrayBindingPattern             < SquareBrackets(Elision? BindingRestElement?) / SquareBrackets(BindingElementList (:"," Elision? BindingRestElement?)?) / SquareBrackets(BindingElementList)
        ArrayBindingPatternYield        < SquareBrackets(Elision? BindingRestElementYield?) / SquareBrackets(BindingElementListYield (:"," Elision? BindingRestElementYield?)?) / SquareBrackets(BindingElementListYield)
        BindingPropertyList             < BindingProperty (:"," BindingProperty)*
        BindingPropertyListYield        < BindingPropertyYield (:"," BindingPropertyYield)*
        BindingElementList              < BindingElisionElement (:"," BindingElisionElement)*
        BindingElementListYield         < BindingElisionElementYield (:"," BindingElisionElementYield)*
        BindingElisionElement           < Elision? BindingElement
        BindingElisionElementYield      < Elision? BindingElementYield
        BindingProperty                 < (PropertyName ":" BindingElement) / SingleNameBinding
        BindingPropertyYield            < (PropertyNameYield ":" BindingElementYield) / SingleNameBindingYield
        BindingElement                  < (BindingPattern InitializerIn?) / SingleNameBinding
        BindingElementYield             < (BindingPatternYield InitializerInYield?) / SingleNameBindingYield
        SingleNameBinding               < BindingIdentifier InitializerIn?
        SingleNameBindingYield          < BindingIdentifierYield InitializerInYield?
        BindingRestElement              < :"..." BindingIdentifier
        BindingRestElementYield         < :"..." BindingIdentifierYield

        EmptyStatement                  < ";"

        ExpressionStatement             < !("{" / (("function" / "class" / ("let" "[")) !(IdentifierStart / IdentifierPart))) ExpressionIn
        ExpressionStatementYield        < !("{" / (("function" / "class" / ("let" "[")) !(IdentifierStart / IdentifierPart))) ExpressionInYield
        ElseKeyword                     < "else"
        IfStatement                     < :"if" Parentheses(ExpressionIn) Statement (ElseKeyword Statement)?
        IfStatementYield                < :"if" Parentheses(ExpressionInYield) StatementYield (ElseKeyword StatementYield)?
        IfStatementReturn               < :"if" Parentheses(ExpressionIn) StatementReturn (ElseKeyword StatementReturn)?
        IfStatementYieldReturn          < :"if" Parentheses(ExpressionInYield) StatementYieldReturn (ElseKeyword StatementYieldReturn)?

        DoKeyword                       <- "do" !(IdentifierStart / IdentifierPart)
        WhileKeyword                    < "while"
        ForKeyword                      < "for"
        VarKeyword                      < "var"
        InKeyword                       < "in"
        OfKeyword                       < "of"
        Semicolon                       < ";"
        IterationStatement              < 
                                            / (DoKeyword Statement WhileKeyword Parentheses(ExpressionIn) Semicolon)
                                            / (WhileKeyword Parentheses(ExpressionIn) Statement)
                                            / (ForKeyword Parentheses(VarKeyword VariableDeclarationList Semicolon ExpressionIn? Semicolon ExpressionIn?) Statement)
                                            / (ForKeyword Parentheses(VarKeyword ForBinding ((InKeyword ExpressionIn) / (OfKeyword AssignmentExpressionIn))) Statement)
                                            / (ForKeyword Parentheses(ForDeclaration InKeyword ExpressionIn) Statement)
                                            / (ForKeyword Parentheses(ForDeclaration OfKeyword AssignmentExpressionIn) Statement)
                                            / (ForKeyword Parentheses(LexicalDeclaration ExpressionIn? Semicolon ExpressionIn?) Statement)
                                            / (ForKeyword Parentheses(!("let" "[") Expression? Semicolon ExpressionIn? Semicolon ExpressionIn?) Statement)
                                            / (ForKeyword Parentheses(!("let" "[") LeftHandSideExpression InKeyword ExpressionIn) Statement)
                                            / (ForKeyword Parentheses(!("let") LeftHandSideExpression OfKeyword AssignmentExpressionIn) Statement)
        IterationStatementYield         < 
                                            / (DoKeyword StatementYield WhileKeyword Parentheses(ExpressionInYield) Semicolon)
                                            / (WhileKeyword Parentheses(ExpressionInYield) StatementYield)
                                            / (ForKeyword Parentheses(VarKeyword VariableDeclarationListYield Semicolon ExpressionInYield? Semicolon ExpressionInYield?) StatementYield)
                                            / (ForKeyword Parentheses(VarKeyword ForBindingYield ((InKeyword ExpressionInYield) / (OfKeyword AssignmentExpressionInYield))) StatementYield)
                                            / (ForKeyword Parentheses(ForDeclarationYield InKeyword ExpressionInYield) StatementYield)
                                            / (ForKeyword Parentheses(ForDeclarationYield OfKeyword AssignmentExpressionInYield) StatementYield)
                                            / (ForKeyword Parentheses(LexicalDeclarationYield ExpressionInYield? Semicolon ExpressionInYield?) StatementYield)
                                            / (ForKeyword Parentheses(!("let" "[") ExpressionYield? Semicolon ExpressionInYield? Semicolon ExpressionInYield?) StatementYield)
                                            / (ForKeyword Parentheses(!("let" "[") LeftHandSideExpressionYield InKeyword ExpressionInYield) StatementYield)
                                            / (ForKeyword Parentheses(!("let") LeftHandSideExpressionYield OfKeyword AssignmentExpressionInYield) StatementYield)
        IterationStatementReturn        < 
                                            / (DoKeyword StatementReturn WhileKeyword Parentheses(ExpressionIn) Semicolon)
                                            / (WhileKeyword Parentheses(ExpressionIn) StatementReturn)
                                            / (ForKeyword Parentheses(VarKeyword VariableDeclarationList Semicolon ExpressionIn? Semicolon ExpressionIn?) StatementReturn)
                                            / (ForKeyword Parentheses(VarKeyword ForBinding ((InKeyword ExpressionIn) / (OfKeyword AssignmentExpressionIn))) StatementReturn)
                                            / (ForKeyword Parentheses(ForDeclaration InKeyword ExpressionIn) StatementReturn)
                                            / (ForKeyword Parentheses(ForDeclaration OfKeyword AssignmentExpressionIn) StatementReturn)
                                            / (ForKeyword Parentheses(LexicalDeclaration ExpressionIn? Semicolon ExpressionIn?) StatementReturn)
                                            / (ForKeyword Parentheses(!("let" "[") Expression? Semicolon ExpressionIn? Semicolon ExpressionIn?) StatementReturn)
                                            / (ForKeyword Parentheses(!("let" "[") LeftHandSideExpression InKeyword ExpressionIn) StatementReturn)
                                            / (ForKeyword Parentheses(!("let") LeftHandSideExpression OfKeyword AssignmentExpressionIn) StatementReturn)
        IterationStatementYieldReturn   < 
                                            / (DoKeyword StatementYieldReturn WhileKeyword Parentheses(ExpressionInYield) Semicolon)
                                            / (WhileKeyword Parentheses(ExpressionInYield) StatementYieldReturn)
                                            / (ForKeyword Parentheses(VarKeyword VariableDeclarationListYield Semicolon ExpressionInYield? Semicolon ExpressionInYield?) StatementYieldReturn)
                                            / (ForKeyword Parentheses(VarKeyword ForBindingYield ((InKeyword ExpressionInYield) / (OfKeyword AssignmentExpressionInYield))) StatementYieldReturn)
                                            / (ForKeyword Parentheses(ForDeclarationYield InKeyword ExpressionInYield) StatementYieldReturn)
                                            / (ForKeyword Parentheses(ForDeclarationYield OfKeyword AssignmentExpressionInYield) StatementYieldReturn)
                                            / (ForKeyword Parentheses(LexicalDeclarationYield ExpressionInYield? Semicolon ExpressionInYield?) StatementYieldReturn)
                                            / (ForKeyword Parentheses(!("let" "[") ExpressionYield? Semicolon ExpressionInYield? Semicolon ExpressionInYield?) StatementYieldReturn)
                                            / (ForKeyword Parentheses(!("let" "[") LeftHandSideExpressionYield InKeyword ExpressionInYield) StatementYieldReturn)
                                            / (ForKeyword Parentheses(!("let") LeftHandSideExpressionYield OfKeyword AssignmentExpressionInYield) StatementYieldReturn)

        ForDeclaration              < LetOrConst ForBinding
        ForDeclarationYield         < LetOrConst ForBindingYield
        ForBinding                  < BindingPattern / BindingIdentifier
        ForBindingYield             < BindingPatternYield / BindingIdentifierYield
        ContinueStatement           <- (("continue" !(IdentifierStart) Spaces LabelIdentifier) / ("continue" !(IdentifierStart / IdentifierPart))) Spaces :";"?
        ContinueStatementYield      <- (("continue" !(IdentifierStart) Spaces LabelIdentifierYield) / ("continue" !(IdentifierStart / IdentifierPart))) Spaces :";"?
        BreakStatement              <- (("break" !(IdentifierStart) Spaces LabelIdentifier) / ("break" !(IdentifierStart / IdentifierPart))) Spaces :";"?
        BreakStatementYield         <- (("break" !(IdentifierStart) Spaces LabelIdentifierYield) / ("break" !(IdentifierStart / IdentifierPart))) Spaces :";"?
        ReturnStatement             <- (("return" !(IdentifierStart) Spaces ExpressionIn) / ("return" !(IdentifierStart / IdentifierPart))) Spaces :";"?
        ReturnStatementYield        <- (("return" !(IdentifierStart) Spaces ExpressionInYield) / ("return" !(IdentifierStart / IdentifierPart))) Spaces :";"?

        WithStatement               < :"with" Parentheses(ExpressionIn) Statement
        WithStatementYield          < :"with" Parentheses(ExpressionInYield) StatementYield
        WithStatementReturn         < :"with" Parentheses(ExpressionIn) StatementReturn
        WithStatementYieldReturn    < :"with" Parentheses(ExpressionInYield) StatementYieldReturn

        SwitchStatement             < :"switch" Parentheses(ExpressionIn) CaseBlock
        SwitchStatementYield        < :"switch" Parentheses(ExpressionInYield) CaseBlockYield
        SwitchStatementReturn       < :"switch" Parentheses(ExpressionIn) CaseBlockReturn
        SwitchStatementYieldReturn  < :"switch" Parentheses(ExpressionInYield) CaseBlockYieldReturn

        CaseBlock                   < CurlyBrackets(CaseClauses? DefaultClause? CaseClauses?)
        CaseBlockYield              < CurlyBrackets(CaseClausesYield? DefaultClauseYield? CaseClausesYield?)
        CaseBlockReturn             < CurlyBrackets(CaseClausesReturn? DefaultClauseReturn? CaseClausesReturn?)
        CaseBlockYieldReturn        < CurlyBrackets(CaseClausesYieldReturn? DefaultClauseYieldReturn? CaseClausesYieldReturn?)

        CaseClauses                 < CaseClause+
        CaseClausesYield            < CaseClauseYield+
        CaseClausesReturn           < CaseClauseReturn+
        CaseClausesYieldReturn      < CaseClauseYieldReturn+

        CaseClause                  < :"case" ExpressionIn :":" StatementList?
        CaseClauseYield             < :"case" ExpressionInYield :":" StatementListYield?
        CaseClauseReturn            < :"case" ExpressionIn :":" StatementListReturn?
        CaseClauseYieldReturn       < :"case" ExpressionInYield :":" StatementListYieldReturn?

        DefaultClause               < :"default" :":" StatementList?
        DefaultClauseYield          < :"default" :":" StatementListYield?
        DefaultClauseReturn         < :"default" :":" StatementListReturn?
        DefaultClauseYieldReturn    < :"default" :":" StatementListYieldReturn?

        LabelledStatement               < LabelIdentifier :":" LabelledItem
        LabelledStatementYield          < LabelIdentifierYield :":" LabelledItemYield
        LabelledStatementReturn         < LabelIdentifier :":" LabelledItemReturn
        LabelledStatementYieldReturn    < LabelIdentifierYield :":" LabelledItemYieldReturn

        LabelledItem                < FunctionDeclaration / Statement
        LabelledItemYield           < FunctionDeclarationYield / StatementYield
        LabelledItemReturn          < FunctionDeclaration / StatementReturn
        LabelledItemYieldReturn     < FunctionDeclarationYield / StatementYieldReturn

        ThrowStatement              <- "throw" !(IdentifierStart) Spaces ExpressionIn Spaces :";"?
        ThrowStatementYield         <- "throw" !(IdentifierStart) Spaces ExpressionInYield Spaces :";"?

        TryStatement                < :"try" %Block Catch? Finally?
        TryStatementYield           < :"try" %BlockYield CatchYield? FinallyYield?
        TryStatementReturn          < :"try" %BlockReturn CatchReturn? FinallyReturn?
        TryStatementYieldReturn     < :"try" %BlockYieldReturn CatchYieldReturn? FinallyYieldReturn?

        Catch                       < :"catch" Parentheses(CatchParameter) %Block
        CatchYield                  < :"catch" Parentheses(CatchParameterYield) %BlockYield
        CatchReturn                 < :"catch" Parentheses(CatchParameter) %BlockReturn
        CatchYieldReturn            < :"catch" Parentheses(CatchParameterYield) %BlockYieldReturn

        Finally                     < :"finally" %Block
        FinallyYield                < :"finally" %BlockYield
        FinallyReturn               < :"finally" %BlockReturn
        FinallyYieldReturn          < :"finally" %BlockYieldReturn

        CatchParameter              < BindingPattern / BindingIdentifier
        CatchParameterYield         < BindingPatternYield / BindingIdentifierYield

        DebuggerStatement               < :"debugger" :";"
        CurlyBrackets(Content)          < "{" Content "}"
        Parentheses(Content)            < "(" Content ")"
        SquareBrackets(Content)         < "[" Content "]"

        FunctionDeclaration                 < (:"function" BindingIdentifier Parentheses(FormalParameters) CurlyBrackets(FunctionBody))
        FunctionDeclarationYield            < (:"function" BindingIdentifierYield Parentheses(FormalParameters) CurlyBrackets(FunctionBody))
        FunctionDeclarationDefault          < (:"function" BindingIdentifier Parentheses(FormalParameters) CurlyBrackets(FunctionBody)) / (:"function" Parentheses(FormalParameters) CurlyBrackets(FunctionBody))
        FunctionDeclarationYieldDefault     < (:"function" BindingIdentifierYield Parentheses(FormalParameters) CurlyBrackets(FunctionBody)) / (:"function" Parentheses(FormalParameters) CurlyBrackets(FunctionBody))
        
        FunctionExpression                  < "function" BindingIdentifier? Parentheses(FormalParameters) CurlyBrackets(FunctionBody)

        StrictFormalParameters          < FormalParameters
        StrictFormalParametersYield     < FormalParametersYield
        FormalParameters                < FormalParameterList?
        FormalParametersYield           < FormalParameterListYield?
        FormalParameterList             < FunctionRestParameter / (FormalsList (:"," FunctionRestParameter)?)
        FormalParameterListYield        < FunctionRestParameterYield / (FormalsListYield (:"," FunctionRestParameterYield)?)
        FormalsList                     < FormalParameter (:"," FormalParameter)*
        FormalsListYield                < FormalParameterYield (:"," FormalParameterYield)*
        FunctionRestParameter           < BindingRestElement
        FunctionRestParameterYield      < BindingRestElementYield
        FormalParameter                 < BindingElement
        FormalParameterYield            < BindingElementYield
        FunctionBody                    < FunctionStatementList
        FunctionBodyYield               < FunctionStatementListYield
        FunctionStatementList           < StatementListReturn?
        FunctionStatementListYield      < StatementListYieldReturn?
        ArrowFunction                   <- ArrowParameters Spaces "=>" Spacing ConciseBody
        ArrowFunctionIn                 <- ArrowParameters Spaces "=>" Spacing ConciseBodyIn
        ArrowFunctionYield              <- ArrowParametersYield Spaces "=>" Spacing ConciseBody
        ArrowFunctionInYield            <- ArrowParametersYield Spaces "=>" Spacing ConciseBodyIn
        ArrowParameters                 < BindingIdentifier / CoverParenthesizedExpressionAndArrowParameterList
        ArrowParametersYield            < BindingIdentifierYield / CoverParenthesizedExpressionAndArrowParameterListYield
        ConciseBody                     < (!("{") AssignmentExpression) / CurlyBrackets(FunctionBody)
        ConciseBodyIn                   < (!("{") AssignmentExpressionIn) / CurlyBrackets(FunctionBody)

        ArrowFormalParameters               < Parentheses(StrictFormalParameters)
        ArrowFormalParametersYield          < Parentheses(StrictFormalParametersYield)
        MethodDefinition                    < GeneratorMethod / ("get" PropertyName "(" ")" CurlyBrackets(FunctionBody)) / ("set" PropertyName Parentheses(PropertySetParameterList) CurlyBrackets(FunctionBody)) / (PropertyName Parentheses(StrictFormalParameters) CurlyBrackets(FunctionBody))
        MethodDefinitionYield               < GeneratorMethodYield / ("get" PropertyNameYield "(" ")" CurlyBrackets(FunctionBody)) / ("set" PropertyNameYield Parentheses(PropertySetParameterList) CurlyBrackets(FunctionBody)) / (PropertyNameYield Parentheses(StrictFormalParameters) CurlyBrackets(FunctionBody))
        PropertySetParameterList            < FormalParameter
        GeneratorMethod                     < :"*" PropertyName Parentheses(StrictFormalParameters) CurlyBrackets(GeneratorBody)
        GeneratorMethodYield                < :"*" PropertyNameYield Parentheses(StrictFormalParametersYield) CurlyBrackets(GeneratorBody)
        GeneratorDeclaration                < :"function" :"*" BindingIdentifier Parentheses(FormalParameters) CurlyBrackets(GeneratorBody)
        GeneratorDeclarationYield           < :"function" :"*" BindingIdentifierYield Parentheses(FormalParametersYield) CurlyBrackets(GeneratorBody)
        GeneratorDeclarationDefault         < :"function" :"*" BindingIdentifier? Parentheses(FormalParameters) CurlyBrackets(GeneratorBody)
        GeneratorDeclarationYieldDefault    < :"function" :"*" BindingIdentifierYield? Parentheses(FormalParametersYield) CurlyBrackets(GeneratorBody)
        GeneratorExpression                 < :"function" :"*" BindingIdentifierYield? Parentheses(FormalParametersYield) CurlyBrackets(GeneratorBody)
        GeneratorBody                       < FunctionBodyYield
        YieldExpression                     <- ("yield" Spaces ("*")? AssignmentExpressionYield) / "yield"
        YieldExpressionIn                   <- ("yield" Spaces ("*")? AssignmentExpressionInYield) / "yield"
        ClassDeclaration                    <- :"class" !(IdentifierStart / IdentifierPart) Spaces BindingIdentifier Spaces ClassTail
        ClassDeclarationYield               <- :"class" !(IdentifierStart / IdentifierPart) Spaces BindingIdentifierYield Spaces ClassTailYield
        ClassDeclarationDefault             <- :"class" !(IdentifierStart / IdentifierPart) Spaces BindingIdentifier? Spaces ClassTail
        ClassDeclarationYieldDefault        <- :"class" !(IdentifierStart / IdentifierPart) Spaces BindingIdentifierYield? Spaces ClassTailYield
        ClassExpression                     <- :"class" !(IdentifierStart / IdentifierPart) Spaces BindingIdentifier? Spaces ClassTail
        ClassExpressionYield                <- :"class" !(IdentifierStart / IdentifierPart) Spaces BindingIdentifierYield? Spaces ClassTailYield
        ClassTail                           < ClassHeritage? CurlyBrackets(ClassBody?)
        ClassTailYield                      < ClassHeritageYield? CurlyBrackets(ClassBodyYield?)
        ClassHeritage                       < :"extends" LeftHandSideExpression
        ClassHeritageYield                  < :"extends" LeftHandSideExpressionYield
        ClassBody                           < ClassElementList 
        ClassBodyYield                      < ClassElementListYield 
        ClassElementList                    < ClassElement+
        ClassElementListYield               < ClassElementYield+
        ClassElement                        < ("static" MethodDefinition) / :";" / MethodDefinition
        ClassElementYield                   < ("static" MethodDefinitionYield) / :";" / MethodDefinitionYield

        JSXElement                      < "<" JSXTag JSXAttributeSet ("/>" / (">" JSXContent "</" JSXTag ">"))
        JSXAttributeSet                 < JSXAttribute*
        JSXAttribute                    < CurlyBrackets(SpreadElement) / (JSXKey ("=" JSXValue)?)
        JSXKey                          <~ (!("=" / "/>" / ">") .)+
        JSXInlineJS                     < ArrowFunction / ConditionalExpression
        JSXValue                        < StringLiteral / JSXCodeBlock
        JSXCodeBlock                    < CurlyBrackets(JSXInlineJS)
        JSXJavaScriptValue              < CurlyBrackets(!JSXElement JSXInlineJS)
        JSXTag                          <~ [a-zA-Z][a-zA-Z0-9]*
        JSXContent                      < (JSXElement / JSXCodeBlock / JSXContentText)*
        JSXContentText                  <~ (!"</" SourceCharacter)*
    `);
}