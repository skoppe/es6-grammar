import std.stdio;
import es6.grammar;
import std.getopt;
version (Stats)
{
	import resusage.memory;	
}

int main(string[] args)
{
	import std.stdio : stdin;
	import std.file : readText, write, exists;
	import std.algorithm : joiner, map;
	import std.stdio : writeln;
	import std.conv : text, to;
	import std.algorithm : remove;
	import std.path : baseName;
	bool time;
	bool forceStdin;
	bool tree;
	string fileIn;
	string fileOut;

	auto helpInformation = getopt(
		args,
		"time", "Show timing information", &time,
		"i|input", "Input file (defaults to stdin)", &fileIn,
		"o|output", "Output file (defaults to stdout)", &fileOut,
		"tree", "Show tree (warning not on big files)", &tree
	);

	if (helpInformation.helpWanted)
	{
		defaultGetoptPrinter("ECMAScript 5 minifier.\n\nUsage:\t"~baseName(args[0])~" [OPTIONS]\n\nOptions:\n", helpInformation.options);
		return 1;
	}

	import std.datetime : StopWatch;
	StopWatch sw;
	string[] timing;
	ParseTree p;
	sw.start();
	if (fileIn.length > 0)
	{
		if (!exists(fileIn))
		{
			writeln("File `",fileIn,"` doesn't exist.");
			assert(false);
		}
		p = ES6(readText(fileIn));
	}
	else
	{
		auto t = stdin.byChunk(4096).map!(c=>cast(char[])c).joiner.to!string;
		p = ES6(t);
	}
	if (!p.successful)
	{
		writeln("Couldn't parse...");
		writeln(p);
		assert(false);
	}
	sw.stop();

	if (tree)
		writeln(p);

	if (time)
		writeln("Ok! - in "~sw.peek().msecs.to!string~"ms");
	else
		writeln("Ok!");
	version (Stats)
	{	
		// first run gc
		import core.memory;

		GC.collect();
		GC.minimize();

		// then report mem
		ProcessMemInfo procMemInfo = processMemInfo();
		writeln("Used ",procMemInfo.usedRAM," RAM");
	}
	return 0;
}